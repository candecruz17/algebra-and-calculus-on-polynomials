package edu.uprm.ece.icom4035.polynomial;

import java.util.StringTokenizer;

import edu.uprm.ece.icom4035.list.ArrayList;
import edu.uprm.ece.icom4035.list.List;

public class TermImp implements Term {

	private double coefficient;
	private int exponent;
	
	public TermImp(double coefficient, int exponent) {
		super();
		this.coefficient = coefficient;
		this.exponent = exponent;
	}
	
	@Override
	public double getCoefficient() {
		// TODO Auto-generated method stub
		return this.coefficient;
	}

	@Override
	public int getExponent() {
		// TODO Auto-generated method stub
		return this.exponent;
	}

	@Override
	public double evaluate(double x) {
		// TODO Auto-generated method stub
		return this.coefficient * Math.pow(x, this.exponent);
	}
	
	public String toString() {
		if(this.exponent == 0) {
			return String.format("%.2f", this.coefficient);
		} else if(this.exponent == 1) {
			return String.format("%.2fx", this.coefficient);
		} else {
			return String.format("%.2fx^%d", this.coefficient, this.exponent);
		}
	}
	
	public static Term fromString(String str) {
		String temp = new String(str);
		TermImp result = null;
		if(temp.contains("x^")) {
			//this is for any term that has an exponent
			StringTokenizer strTok = new StringTokenizer(temp, "x^");
			List<String> list = new ArrayList<String>(2);
			while(strTok.hasMoreElements()) {
				list.add((String) strTok.nextElement());
			}
			if(list.size() == 0) {
				throw new IllegalArgumentException("Argument string is formatted illegally");
			} else if(list.size() == 1) {
				//applies to the term that has an exponent of n
				Integer exp = Integer.parseInt(list.get(0));
				result = new TermImp(1, exp);
			} else {
				//for the term that is of the form ax^n
				Double coef = Double.parseDouble(list.get(0));
				Integer exp = Integer.parseInt(list.get(1));
				result = new TermImp(coef, exp);
			}
		} else if(temp.contains("x")) {
			//for those that have exponent of 1
			StringTokenizer strTok = new StringTokenizer(temp, "x");
			List<String> list = new ArrayList<String>(2);
			while(strTok.hasMoreTokens()) {
				list.add((String) strTok.nextElement());
			}
			if(list.size() == 0) {
				result = new TermImp(1.0, 1);
			} else {
				Double coef = Double.parseDouble(list.get(0));
				result = new TermImp(coef, 1);
			}
		} else {
			//numeric values
			result = new TermImp(Double.parseDouble(temp), 0);
		}
		return result;
	}

}
