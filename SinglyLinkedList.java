package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SinglyLinkedList<E> implements List<E> {

	private static class Node<E>{
		private E element;
		private Node<E> next;
		
		public Node() {
			element = null;
			next = null;
		}
		
		public Node(E element) {
			this.element = element;
			next = null;
		}
		public Node(E element, Node<E> next) {
			this.element = element;
			this.next = next;
		}
		
		public E getElement() {
			return element;
		}
		
		public void setElement(E element) {
			this.element = element;
		}
		
		public Node<E> getNext(){
			return next;
		}
		
		public void setNext(Node<E> next) {
			this.next = next;
		}
	}
	
	private Node<E> dHeader;
	private int size;
	
	public SinglyLinkedList() {
		dHeader = new Node<E>();
		size = 0;
	}
	
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
	
		return new SLLIterator<>(this);
	}
	
	private class SLLIterator<E> implements Iterator<E> {
		private SinglyLinkedList<E> list;
		private int counter = 0;
		
		public SLLIterator(SinglyLinkedList<E> list) {
			this.list = list;
		}

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			if(counter < size()) {
				return false;
			}
			return false;
		}

		@Override
		public E next() {
			// TODO Auto-generated method stub
			if(!this.hasNext()) {
				throw new NoSuchElementException("There is no such element");
			}
			return list.get(counter++);
		}
	}

	@Override
	public void add(E obj) {
		// TODO Auto-generated method stub
		Node<E> current = dHeader;
		while(current.getNext() != null) {
			current = current.getNext();
		}
		current.setNext(new Node<E>(obj));
		size++;
	}

	@Override
	public void add(int index, E obj) {
		// TODO Auto-generated method stub
		int counter = 0;
		Node<E> nodeToAdd = new Node<E>(obj);
		Node<E> current = dHeader;
		if(index == 0) {
			nodeToAdd.setNext(dHeader.getNext());
			dHeader.setNext(nodeToAdd);
		} else {
			while(counter < index - 1) {
				current = current.getNext();
				counter++;
			}
			nodeToAdd.setNext(current.getNext());
			current.setNext(nodeToAdd);
		}
		size++;
	}

	@Override
	public boolean remove(E obj) {
		// TODO Auto-generated method stub
		if(size == 0) {
			return false;
		}
		Node<E> prev = dHeader;
		Node<E> current = dHeader.getNext();
		while(current.getNext() != null && current.getNext() != obj) {
			prev = current;
			current = current.getNext();
			if(current.getElement().equals(obj)) {
				prev.setNext(current.getNext());
				size--;
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean remove(int index) {
		// TODO Auto-generated method stub
		if(size == 0 || index > size || index < 0) {
			throw new IndexOutOfBoundsException("Out of bounds");
		}
		int counter = 0;
		Node<E> prev = dHeader;
		Node<E> nodeToRemove = dHeader.getNext();
		while(counter < index) {
			prev = nodeToRemove;
			nodeToRemove = nodeToRemove.getNext();
			counter++;
		}
		prev.setNext(nodeToRemove.getNext());
		size--;
		return true;
	}

	@Override
	public int removeAll(E obj) {
		// TODO Auto-generated method stub
		int counter = 0;
		Node<E> current = dHeader.getNext();
		Node<E> prev = dHeader;
		while(current.getNext() != null) {
			if(current.getElement().equals(obj)) {
				prev.setNext(current.getNext());
				current = prev.getNext();
				size--;
				counter++;
			} else {
				prev = current;
				current = current.getNext();
			}
		}
		return counter;
	}

	@Override
	public E get(int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index > size || size == 0) {
			throw new IndexOutOfBoundsException("Out of bounds");
		}
		int counter = 0;
		Node<E> elementToGet = dHeader.getNext();
		while(counter != index) {
			elementToGet = elementToGet.getNext();
			counter++;
		}
		return elementToGet.getElement();
	}

	@Override
	public E set(int index, E obj) {
		// TODO Auto-generated method stub
		if(index < 0 || index > size || size == 0) {
			throw new IndexOutOfBoundsException("Out of bounds");
		}
		int counter = 0;
		Node<E> prev = dHeader;
		Node<E> current = dHeader.getNext();
		while(counter < index && current.getNext() != null) {
			prev = current;
			current = current.getNext();
			counter++;
		}
		E elementToRemove = current.getElement();
		prev.setNext(new Node<E>(obj, current.getNext()));
		return elementToRemove;
	}

	@Override
	public E first() {
		// TODO Auto-generated method stub
		if(size == 0) {
			return null;
		}
		return dHeader.getNext().getElement();
	}

	@Override
	public E last() {
		// TODO Auto-generated method stub
		if(size == 0) {
			return null;
		}
		Node<E> elementToRemove = dHeader.getNext();
		while(elementToRemove.getNext() != null) {
			elementToRemove = elementToRemove.getNext();
		}
		return elementToRemove.getElement();
	}

	@Override
	public int firstIndex(E obj) {
		// TODO Auto-generated method stub
		if(size == 0) {
			return -1;
		}
		int counter = 0;
		Node<E> nodeToFind = dHeader.getNext();
		while(nodeToFind.getNext() != null) {
			if(nodeToFind.getElement().equals(obj)) {
				return counter;
			}
			nodeToFind = nodeToFind.getNext();
			counter++;
		}
		if(nodeToFind.getElement().equals(obj)) {
			return counter;
		}
		return -1;
	}

	@Override
	public int lastIndex(E obj) {
		// TODO Auto-generated method stub
		if(size == 0) {
			return -1;
		}
		int lastIndex = 0;
		int counter = 0;
		Node<E> current = dHeader.getNext();
		Node<E> index = dHeader.getNext();
		while(current.getNext() != null) {
			if(current.getElement().equals(obj)) {
				index = current;
				lastIndex = counter;
				counter++;
				current = current.getNext();
			} else {
				counter++;
				current = current.getNext();
			}
		}
		if(index.getElement().equals(obj)) {
			return lastIndex;
		}
		return -1;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return this.size() == 0;
	}

	@Override
	public boolean contains(E obj) {
		// TODO Auto-generated method stub
		Node<E> nodeToFind = dHeader.getNext();
		while(nodeToFind.getNext() != null) {
			if(nodeToFind.getElement().equals(obj)) {
				return true;
			}
			nodeToFind = nodeToFind.getNext();
		}
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		dHeader.setNext(null);
		size = 0;
	}
}
